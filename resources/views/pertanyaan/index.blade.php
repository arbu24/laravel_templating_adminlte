@extends('master')

@push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets-adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets-adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('pertanyaan_active')
    active
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Pertanyaan</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Pertanyaan</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="/pertanyaan/create"><button type="button" class="btn btn-primary">+ Tambah Pertanyaan</button></a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          @if(session('success'))
                              <div class="alert alert-success">
                                {{session('success')}}
                              </div>
                          @endif
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>No</th>
                              <th>Judul</th>
                              <th>Isi</th>
                              <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                              @forelse ($query as $item => $p)
                              <tr>
                                  <td>{{$item + 1}}</td>
                                  <td>{{$p -> judul}}</td>
                                  <td>{{$p -> isi}}</td>
                                  <td>
                                      <a class="btn btn-outline-info" href="/pertanyaan/{{$p->id}}">Detail</a>
                                      <a class="btn btn-outline-warning" href="/pertanyaan/{{$p->id}}/edit">Edit</a>
                                      <form action="/pertanyaan/{{$p->id}}" method="POST" style="display: inline-block">
                                        @csrf
                                        @method('DELETE')  
                                        <input type="submit" name="delete" id="delete" value="Hapus" class="btn btn-outline-danger">
                                      </form>
                                  </td>
                              </tr>
                              @empty
                              <tr>
                                  <td colspan="4" align="center">Tidak ada pertanyaan</td>
                              </tr>
                              @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>No</th>
                              <th>Judul</th>
                              <th>Isi</th>
                              <th>Action</th>
                            </tr>
                            </tfoot>
                          </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>
      <!-- /.content -->    
@endsection

@push('script')
    <script src="{{asset('assets-adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets-adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush