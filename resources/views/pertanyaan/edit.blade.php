@extends('master')

@push('style')
    
@endpush

@section('pertanyaan_active')
    active
@endsection

@section('content')

    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Pertanyaan</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Pertanyaan</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-warning">
                        <div class="card-header">
                        <h3 class="card-title">Edit Pertanyaan</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="/pertanyaan/{{$query->id}}" method="POST">
                        <div class="card-body">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text" class="form-control" name="judul" id="judul" value="{{ old('judul',$query->judul) }}" placeholder="Masukan Judul">
                                @error('judul')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="isi">Isi</label>
                                <textarea class="form-control" name="isi" value="{{ old('isi','') }}" placeholder="Silahkan tulis disini ..." id="isi" rows="5">{{ $query->isi}}</textarea>
                                @error('isi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- /.card-body -->
                    
                        <div class="card-footer">
                            <button type="submit" class="btn btn-warning">Update</button>
                            <a class="btn btn-danger my-2" href="/pertanyaan">Kembali</a>
                        </div>
                        </form>
                    </div>
                </div>
                {{-- <div class="col-md-3 offset-md-1"> --}}
                <div class="col-md-3">
                    <div class="card border border-succes mb-3">
                        <div class="card-header">
                            <h3 class="card-title">Catatan</h3>
                        </div>
                        <div class="card-body text-success">
                            Nothing special here,<br> 
                            just input your title in field <i><strong>judul</strong></i> and input your question in field <i><strong>Isi</strong></i>
                            in the form. <br>
                            click Update.
                            hehe :)
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </section>

@endsection

@push('script')
    
@endpush

