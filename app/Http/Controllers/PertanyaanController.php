<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{

    public function index(){
        // $query = DB::table('pertanyaans')->get();
        $query = Pertanyaan::all();
        // dd($query);
        return view('pertanyaan.index',compact('query'));
    }

    public function create(){
        return view ('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            "judul" => 'required|unique:pertanyaans',
            "isi"   => 'required',
        ]);

        // $query = DB::table('pertanyaans')->insert([
        //     "judul" => $request['judul'],
        //     "isi"   => $request['isi']
        // ]);

        $simpan = Pertanyaan::create([
            'judul' => $request['judul'],
            'isi'   => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }

    public function show($id){
        // $query = DB::table('pertanyaans')
        //         ->where('id',$id)
        //         ->first(); //klo pake get()  mengahasilkan collection
        $query = Pertanyaan::find($id);

        // dd($query);
        return view('pertanyaan.detail',compact('query'));
    }

    public function edit($id){
        // $query = DB::table('pertanyaans')->where('id',$id)->first();
        $query = Pertanyaan::find($id);
        // dd($query);
        return view('pertanyaan.edit',compact('query'));
    }

    public function update($id, Request $request){
        // $query = DB::table('pertanyaans')
        //             ->where('id',$id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'isi'  => $request['isi']
        //             ]);
        $query = Pertanyaan::find($id);
        $query ->judul = $request->judul;
        $query ->isi = $request->isi;
        $query ->update();
        
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil di Edit!');
    }

    public function destroy($id){
        // $query = DB::table('pertanyaans')
        //             ->where('id',$id)
        //             ->delete();

        $query = Pertanyaan::find($id);
        $query->delete();
                    
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil di Hapus!');
    }
}
